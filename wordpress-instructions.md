### Instructions to include auth.js in a WordPress website

To add support for auth.js and the keybase-like authentication scheme used by auth.js in a
WordPress website, the following modifications/additions need to be made in the WordPress source code.
The changed files of the WordPress back-end can be found in this repository.

# Server-side authentication
The following changes need to be made to the WordPress authentication system to make it able to
support authenticating users with the new authentication scheme:

   1. The default authentication filter needs to be changed to call the authjs_authenticate function instead of the default WordPress authentication function. This change needs to be made in the default-filters.php file (default-filters.php: lines 431-434)
   2. The authjs_authenticate function needs to be added in the user.php file. This function works similarly to the default WordPress authentication function, but calls the check_public_key function to verify the user's credentials, instead of the default wp_check_password function. (user.php:lines 34-89)
   3. The check_public_key function needs to be added in the pluggable.php file. This function checks if the public key sent by the user matches the stored public key and then verifies if the signature of thesent nonce is correct using an external python script (verify.py). The original value of the nonce is retrieved from a cookie which was set when the authentication form was sent to the user. The verify.py script can also be found in the repository. (pluggable.php: lines 2314-2362)

# Front-end source code
auth.js needs to be sent to the client and the authentication and registration forms need to be modified
to generate the correct credentials for the authentication scheme. The following steps must be taken:

   1. auth.js needs to be added in the wp-includes folder and sent to the user. This can be done using the  login_enqueue_scripts action in the wp-login.php file (wp-login.php: lines 118-123)
   2. The wp-login.php needs to be modified to generate and add a random nonce as a cookie. This nonce also needs to be added in the authentication form sent to the user to be signed by auth.js using the user's private key. (wp-login.php: lines 25-27, 1415)
   3. The login and registration forms need to be modified to use the auth.js API calls to change the  typed user's password to the correct credential. An example of how this can be done with JavaScript by deferring the forms' submission and changing the values using auth.js can be seen in the form_submit.js and reset_pass.js files.
 
**Note:** in order for the advanced authentication system to work, the underlying browser needs to 
support the creation of Ed25519 key pairs and signatures and scrypt hashes.
