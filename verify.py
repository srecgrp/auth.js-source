#!/usr/bin/env python
""" Verify ed25519 signature"""

import sys
import nacl.encoding
import nacl.signing

if len(sys.argv) != 4:
    print("Usage: verify.py <public_key> <message> <signature>")
    exit(0)

PUBLIC_KEY_STR = str(sys.argv[1])
MESSAGE_STR = str(sys.argv[2])
SIGNATURE_STR = str(sys.argv[3])

SIGNATURE = bytearray.fromhex(SIGNATURE_STR)
MESSAGE = bytearray.fromhex(MESSAGE_STR)

# Create a VerifyKey object from a hex serialized public key
VERIFY_KEY = nacl.signing.VerifyKey(PUBLIC_KEY_STR, encoder=nacl.encoding.HexEncoder)

# Check the validity of a message's signature
try:
    VERIFIED = VERIFY_KEY.verify(MESSAGE_STR, SIGNATURE_STR, encoder=nacl.encoding.HexEncoder)
    print("1")
except nacl.exceptions.BadSignatureError:
    print("0")
