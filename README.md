# README #

This is the code for the paper "auth.js: Advanced Authentication for the Web", which was accepted for publication in the proceedings of ETAA 2020 (3rd International Workshop on Emerging Technologies for Authorization and Authentication).

### Contents ###

* The auth.js framework source code
* Modifications made to a WordPress website to include the advanced authentication scheme supported by auth.js
* Instructions on how to make these modifications in WordPress.


