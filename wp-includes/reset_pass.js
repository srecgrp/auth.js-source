// Similarly to the login form, this script changes the credential
// when a user first registers their password
jQuery("#resetpassform").on("submit", function (e) {
    e.preventDefault(); //Stop form submission
    let self = jQuery(this);
    initializeCredentialType({
     passwordMinLength: 8,
     passwordProccessMethod: "scrypt_seed_ed25519_keypair",
    });
    let password = jQuery("#pass1").val();
    let public_key = register(password); //Generate the credential using auth.js
    public_key.then( (pk) => {
        console.log(pk);
        jQuery("#pass1").val(pk); //Set the new credential value to be submitted
        jQuery("#pass2").val(pk);
        jQuery("#resetpassform").off("submit");
        self.submit();//Submit the form
    })
});
