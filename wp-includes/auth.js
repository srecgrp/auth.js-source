// auth.js implementation
// Note: We implemented the ed25519 keypair generation and signing process are implemented
// as digest functions in Firefox - this is why they are called using the digest WebCryptoAPI call
// in this source code. This should change when the cryptographic primitives are correctly and
// fully implemented in the browser

let CredentialType = {
  passwordMinLength: null,
  passwordProccessMethod: "plain",
};

let supportedMethods = ["plain", "scrypt_seed_ed25519_keypair"];

let hexToBytes = function (hex) {
  let bytes;
  for (bytes = [], c = 0; c < hex.length; c += 2)
    bytes.push(parseInt(hex.substr(c, 2), 16));
  return bytes;
};

let createKeyPairFromPassword = async function (password) {
  const encoder = new TextEncoder();

  //Get scrypt hash of password
  const passEnc = encoder.encode(password);
  const hashScrypt = await crypto.subtle.digest("SCRYPT", passEnc);
  const hashArrayScrypt = Array.from(new Uint8Array(hashScrypt)); // convert buffer to byte array

  //Slice scrypt hash to 32 bytes to get the privateKey
  const privateKeyArray = hashArrayScrypt.slice(0, 32);
  const privateKey = privateKeyArray
    .map((b) => b.toString(16).padStart(2, "0"))
    .join("");

  const privateKeyEnc = hexToBytes(privateKey);
  const privateKeyBytes = new Uint8Array(privateKeyEnc);
  //Curve25519 scalar mult lower 32 bytes of scrypt hash to get public key
  const publicKeyPromise = await crypto.subtle.digest(
    "CURVE25519",
    privateKeyBytes
  );
  const publicKeyByteArray = Array.from(new Uint8Array(publicKeyPromise)); // convert buffer to byte array

  const publicKey = publicKeyByteArray
    .map((b) => b.toString(16).padStart(2, "0"))
    .join("");

  return privateKey.concat(publicKey);
};

let signMessage = async function (keypair, message) {
  const input = keypair.concat(message);
  const inputBuf = hexToBytes(input);
  const inputArray = new Uint8Array(inputBuf);
  return crypto.subtle.digest("ED25519SIGN", inputArray);
};

function initializeCredentialType(credentialTypeInput) {
  for (key in credentialTypeInput) {
    if (key === "passwordMinLength") {
      if (Number.isInteger(credentialTypeInput.passwordMinLength)) {
        CredentialType.passwordMinLength =
          credentialTypeInput.passwordMinLength;
      } else {
        throw "passwordMinLength must be an integer";
      }
    } else if (key === "passwordProccessMethod") {
      if (
        supportedMethods.includes(credentialTypeInput.passwordProccessMethod)
      ) {
        CredentialType.passwordProccessMethod =
          credentialTypeInput.passwordProccessMethod;
      } else {
        let errorMessage =
          credentialTypeInput.passwordProccessMethod + ": method not supported";
        throw errorMessage;
      }
    } else {
      let errorMessage = key + ": unknown field";
      throw errorMessage;
    }
  }
}

async function register(password) {
    let retVal;
    // Check that the password is longer than passwordMinLength
    if (!(CredentialType.passwordMinLength === null)) {
        if (password.length < CredentialType.passwordMinLength) {
            // If the plain scheme is used, simply return the password
            let errorMessage = "Password must be at least " + CredentialType.passwordMinLength + " characters long";
            throw errorMessage;
        }
    }
    if (CredentialType.passwordProccessMethod === "plain") {
        retVal = password;
    } else if (
        // If scrypt_seed_ed25519_keypair is used
        CredentialType.passwordProccessMethod === "scrypt_seed_ed25519_keypair"
    ) {
        // Create the keypair using the password as a seed and return the public key
        let keypair = await createKeyPairFromPassword(password);
        let public_key = keypair.substr(64, 128);
        retVal = public_key;
    }
    return Promise.resolve(retVal);
}

async function authenticate(password, message = "") {
    let retVal;
    // Get the chosen authentication method
    if (CredentialType.passwordProccessMethod === "plain") {
        // If the plain scheme is used, simply return the password
        retVal = password;
    } else if (
        // If scrypt_seed_ed25519_keypair is used
        CredentialType.passwordProccessMethod === "scrypt_seed_ed25519_keypair"
    ) {
        // Create the keypair using the password as a seed
        let keypair = await createKeyPairFromPassword(password);
        // Get the public key part of the key pair
        let public_key = keypair.substr(64, 128);
        // Sign the message
        let signedMessageBytes = await signMessage(keypair, message);
        let signedMessageArray = Array.from(new Uint8Array(signedMessageBytes));
        let signature = signedMessageArray
          .map((b) => b.toString(16).padStart(2, "0"))
          .join("");
        // Return the public key concatenated with the signature
        retVal = public_key.concat(signature);
    }
    return Promise.resolve(retVal);
}
