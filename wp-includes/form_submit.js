// This defers the login form submission and changes the typed password
// to the correct credential, which in this case is the ed25519 public 
// key and the signature of the nonce
jQuery("#loginform").on("submit", function (e) {
    e.preventDefault(); // Stop form submission
    let self = jQuery(this);
    initializeCredentialType({
     passwordMinLength: 8,
     passwordProccessMethod: "scrypt_seed_ed25519_keypair",
    });
    let password = jQuery("#user_pass").val();
    let message = jQuery("#nonce-message").val();
    let credentials = authenticate(password, message);
    credentials.then( (cred) => {
        jQuery("#user_pass").val(cred); //change the typed password with the correct credential
        jQuery("#loginform").off("submit");
        self.submit();
    })
});
